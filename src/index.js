require("dotenv").config()

const express = require("express");
const viewEngine = require("./config/viewEngine")
const route = require("./routes/index")
const bodyParser = require("body-parser")

const port = process.env.PORT || 8008

const app = express()

viewEngine(app)

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true }))

route(app)

app.listen(port, () => {
    console.log(`chatbot running at http://localhost:${port}`)
})