const express = require('express')
const {getHomePage, getWebhook, postWebhook} = require('../controllers/chatbotControllers')
const router = express.Router()

const route = (app) => {
    router.get('/', getHomePage)

    router.get('/webhook', getWebhook)
    router.post('/webhook', postWebhook)

    return app.use('/', router)
}

module.exports = route