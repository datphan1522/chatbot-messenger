const { response, request } = require('express')
const requests = require('request')
require('dotenv').config()

const PAGE_ACCESS_TOKEN= process.env.PAGE_ACCESS_TOKEN
const VERIFY_TOKEN = process.env.VERIFY_TOKEN

const getHomePage = (req, res) => {
    return res.send("Xin chao!")
}

const postWebhook = (req, res) => {
    let body = req.body

    if (body.object === "page") {

        body.entry.forEach(entry => {

            let webhook_event = entry.messaging[0]
            console.log(webhook_event);

            let sender_psid =webhook_event.sender.id
            console.log('sender PSID: '+ sender_psid);

            if( webhook_event.message){
                handleMessage(sender_psid, webhook_event.message)
            }else if(webhook_event.postback){
                handlePostback(sender_psid, webhook_event.postback)
            }
        });
        // Returns a '200 OK' response to all requests
        res.status(200).send("EVENT_RECEIVED");
        // Determine which webhooks were triggered and get sender PSIDs and locale, message content and more.

    } else {
        // Return a '404 Not Found' if event is not from a page subscription
        res.sendStatus(404);
    }
}

const getWebhook = (req, res) => {

    // Parse the query params
    let mode = req.query["hub.mode"];
    let token = req.query["hub.verify_token"];
    let challenge = req.query["hub.challenge"];

    // Check if a token and mode is in the query string of the request
    if (mode && token) {
        // Check the mode and token sent is correct
        if (mode === "subscribe" && token === VERIFY_TOKEN) {
            // Respond with the challenge token from the request
            console.log("WEBHOOK_VERIFIED");
            res.status(200).send(challenge);
        } else {
            // Respond with '403 Forbidden' if verify tokens do not match
            res.sendStatus(403);
        }
    }
}

const handleMessage = (sender_psid, received_message) => {
    let response
    if(received_message.text){
        response = {
            "text": `Bạn gửi tin nhắn: "${received_message.text}". Bây giờ hay gửi một bức ảnh!`
        }
    }
    else if( received_message.attachments){

        let attachment_url = received_message.attachments[0].payload.url
        response = {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [{
                        "title": "Dây có phải bức ảnh của bạn không?",
                        "subtitle": "Nhấn nút bên dưới để trả lời",
                        "image_url": attachment_url,
                        "buttons": [
                            {
                                "type": "postback",
                                "title": "Có",
                                "payload": "yes",
                            },
                            {
                                "type": "postback",
                                "title": "Không",
                                "payload": "no",
                            }
                        ]
                    }]
                }
            }
        }
    }
    callSendAPI(sender_psid,response)
}
const handlePostback = (sender_psid, received_postback) => {
    let response

    let payload = received_postback.payload

    if(payload ==='yes'){
        response = {"text": "Cảm ơn bạn !"}
    } else if(payload ==='no'){
        response = {"text": "Bạn hãy thử gửi lại một ảnh khác !"}
    }

    callSendAPI(sender_psid,response)
}
const callSendAPI = async (sender_psid, response) => {
    let request_body = {
        "recipient": {
            "id": sender_psid
        },
        "message": response 
    }

    //send the HTTP request to the Messenger Platform
    await requests({
        "uri": "https://graph.facebook.com/v17.0/me/messages",
        "qs": { "access_token": PAGE_ACCESS_TOKEN},
        "method": "POST",
        "json": request_body
    }, (err,res,body) => {
        if(!err) console.log("Message sent.");
        else{
            console.log("Unable to send message: "+ err);
        }
    })
}

module.exports = {
    getHomePage,
    getWebhook,
    postWebhook
}